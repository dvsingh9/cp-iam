package com.codemiro.iam.web;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author dvsingh9
 *
 */
@Controller
public class CustomerController {

	  @GetMapping(path = "/customers")
	    public String customers(Principal principal, Model model) {
	        //addCustomers();
	        //Iterable<Customer> customers = customerDAO.findAll();
	        //model.addAttribute("customers", null);
	        model.addAttribute("username", principal.getName());
	        return "customers";
	    }
}
